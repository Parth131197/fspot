webpackJsonp([0],{

/***/ 119:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 119;

/***/ }),

/***/ 161:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 161;

/***/ }),

/***/ 204:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__signup_signup__ = __webpack_require__(205);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__first_first__ = __webpack_require__(52);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_database_database__ = __webpack_require__(42);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_camera__ = __webpack_require__(53);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_native_file__ = __webpack_require__(54);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ionic_native_transfer__ = __webpack_require__(55);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__ionic_native_file_path__ = __webpack_require__(56);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__providers_service_service__ = __webpack_require__(57);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};











var HomePage = /** @class */ (function () {
    function HomePage(navCtrl, database, alertCtrl, menuCtrl, camera, transfer, file, filePath, actionSheetCtrl, toastCtrl, platform, loadingCtrl, service) {
        //for 2 types of menus
        // this.menuCtrl.enable(true,"beforeLogin");  
        // this.menuCtrl.enable(false,"afterLogin");
        this.navCtrl = navCtrl;
        this.database = database;
        this.alertCtrl = alertCtrl;
        this.menuCtrl = menuCtrl;
        this.camera = camera;
        this.transfer = transfer;
        this.file = file;
        this.filePath = filePath;
        this.actionSheetCtrl = actionSheetCtrl;
        this.toastCtrl = toastCtrl;
        this.platform = platform;
        this.loadingCtrl = loadingCtrl;
        this.service = service;
        this.splash = true;
        this.developers = [];
        this.developer = {};
        this.lastImage = null;
        this.temp = "file:///storage/sdcard0/Android/data/io.ionic.starter/files/a.jpg";
        this.GetAllUser();
    }
    ////////////////////////////////////////////////////////////////////////
    HomePage.prototype.presentActionSheet = function () {
        var _this = this;
        var actionSheet = this.actionSheetCtrl.create({
            title: 'Select Image Source',
            buttons: [
                {
                    text: 'Load from Library',
                    handler: function () {
                        _this.takePicture(_this.camera.PictureSourceType.PHOTOLIBRARY);
                    }
                },
                {
                    text: 'Use Camera',
                    handler: function () {
                        _this.takePicture(_this.camera.PictureSourceType.CAMERA);
                    }
                },
                {
                    text: 'Cancel',
                    role: 'cancel'
                }
            ]
        });
        actionSheet.present();
    };
    HomePage.prototype.takePicture = function (sourceType) {
        var _this = this;
        // Create options for the Camera Dialog
        var options = {
            quality: 100,
            sourceType: sourceType,
            saveToPhotoAlbum: false,
            correctOrientation: true,
            targetWidth: 400,
            targetHeight: 400,
            destinationType: this.camera.DestinationType.FILE_URI
        };
        // Get the data of an image
        this.camera.getPicture(options).then(function (imagePath) {
            // Special handling for Android library
            if (_this.platform.is('android') && sourceType === _this.camera.PictureSourceType.PHOTOLIBRARY) {
                _this.filePath.resolveNativePath(imagePath)
                    .then(function (filePath) {
                    var correctPath = filePath.substr(0, filePath.lastIndexOf('/') + 1);
                    var currentName = imagePath.substring(imagePath.lastIndexOf('/') + 1, imagePath.lastIndexOf('?'));
                    _this.copyFileToLocalDir(correctPath, currentName, _this.createFileName());
                    _this.fileSavePath = cordova.file.externalDataDirectory;
                    console.log("Image Path:" + correctPath + "  Name:" + currentName);
                });
            }
            else {
                var currentName = imagePath.substr(imagePath.lastIndexOf('/') + 1);
                var correctPath = imagePath.substr(0, imagePath.lastIndexOf('/') + 1);
                _this.copyFileToLocalDir(correctPath, currentName, _this.createFileName());
                console.log("Image Path:" + correctPath + "  Name:" + currentName);
                _this.fileSavePath = cordova.file.externalDataDirectory;
            }
        }, function (err) {
            _this.presentToast('Error while selecting image.');
        });
    };
    HomePage.prototype.createFileName = function () {
        var d = new Date(), n = d.getTime(), newFileName = n + ".jpg";
        return newFileName;
    };
    // Copy the image to a local folder
    HomePage.prototype.copyFileToLocalDir = function (namePath, currentName, newFileName) {
        var _this = this;
        console.log("Copying file to local ");
        this.file.copyFile(namePath, currentName, cordova.file.externalDataDirectory, newFileName).then(function (success) {
            //this.lastImage = newFileName;
            //this.fileSavePath=cordova.file.dataDirectory;
            // this.presentToast(' file Stored in.'+cordova.file.dataDirectory);
        }, function (error) {
            _this.presentToast('Error while storing file.');
        });
    };
    HomePage.prototype.presentToast = function (text) {
        var toast = this.toastCtrl.create({
            message: text,
            duration: 3000,
            position: 'top'
        });
        toast.present();
    };
    // Always get the accurate path to your apps folder
    HomePage.prototype.pathForImage = function (img) {
        if (img === null) {
            return '';
        }
        else {
            return cordova.file.dataDirectory + img;
        }
    };
    ////////////////////////////////////////////////////////
    HomePage.prototype.ionViewDidLoad = function () {
        var _this = this;
        setTimeout(function () {
            _this.splash = false;
        }, 3000);
    };
    HomePage.prototype.TakePhoto = function () {
        /*
         this.camera.getPicture({
       destinationType: this.camera.DestinationType.FILE_URI,
       sourceType: sourceType,
       mediaType: this.camera.MediaType.PICTURE,
       encodingType: this.camera.EncodingType.JPEG,
       saveToPhotoAlbum: (source === PictureSource.CAMERA),
       allowEdit: true
     })
       .then(imageUri => {
         this.imageResizer.resize({
           uri: imageUri,
           quality: 60,
           width: 1280,
           height: 1280
         }).then(uri => handler(uri))
       })
       .catch(error => console.warn(error))
     }
     
     
         */
        var options = {
            quality: 100,
            destinationType: this.camera.DestinationType.DATA_URL,
            encodingType: this.camera.EncodingType.JPEG,
            mediaType: this.camera.MediaType.PICTURE
        };
        this.camera.getPicture(options).then(function (imageData) {
            // imageData is either a base64 encoded string or a file URI
            // If it's base64:
            var base64Image = 'data:image/jpeg;base64,' + imageData;
        }, function (err) {
            // Handle error
        });
    };
    HomePage.prototype.DeleteAllUser = function () {
        this.database.DeleteAllUser();
    };
    HomePage.prototype.DeleteAllImages = function () {
        this.database.DeleteAllImages();
    };
    HomePage.prototype.CreateUser = function () {
        this.database.CreateUser("Parth", "parth@parth.com", "kevin", "123456").then(function (data) {
            console.log(data);
        }, function (error) {
            console.log(error);
        });
    };
    HomePage.prototype.GetAllUser = function () {
        var _this = this;
        this.database.GetAllUsers().then(function (data) {
            _this.developers = data;
            console.log(data);
        }, function (error) {
            console.log(error);
        });
    };
    HomePage.prototype.signup = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__signup_signup__["a" /* SignupPage */]);
    };
    HomePage.prototype.login = function (name, pass) {
        var _this = this;
        console.log("Name:" + name + " Pass:" + pass);
        this.database.getUser(name, pass).then(function (data) {
            console.log(data);
            if (data > 0) {
                _this.service.setService(data, name);
                _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__first_first__["a" /* FirstPage */], { data: name });
            }
            else {
                var alert_1 = _this.alertCtrl.create({
                    subTitle: "Invalid username or password"
                });
                alert_1.present();
            }
        }, function (error) {
            console.log(error);
        });
    };
    HomePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-home',template:/*ion-inline-start:"/home/parth/parth/testApp3/src/pages/home/home.html"*/'<div id="custom-overlay" [style.display]="splash ? \'flex\': \'none\'">\n    <div class="flb">\n      <div class="Aligner-item Aligner-item--top"></div>\n      <img src="assets/imgs/logoMM2.png" >\n      <div class="Aligner-item Aligner-item--bottom"></div>\n    </div>\n  </div>\n\n\n<ion-header>\n  <ion-navbar color="dark">\n    <ion-title>\n      FavSpot\n    </ion-title>\n\n    \n    <!-- <button ion-button icon-only menuToggle end>\n        <ion-icon name=\'menu\'></ion-icon>\n    </button>                 for menu button on login page.(2 menu has been created)-->                      \n    \n  </ion-navbar>\n\n</ion-header>\n\n<ion-content padding >\n<div>\n  <div class="maindiv">\n  <ion-list>\n    <ion-item>\n      <ion-label floating>Username</ion-label>\n      <ion-input type="text" #name></ion-input>\n    </ion-item>\n    <ion-item>\n        <ion-label floating>Password</ion-label>\n        <ion-input type="password" #pass></ion-input>\n      </ion-item>\n  </ion-list>\n  \n  \n  <button ion-button round (click)="login(name.value,pass.value)" id="loginbutton">Login</button>\n \n  \n  <button ion-button round (click)="signup()" id="signupbutton">Sign up</button>\n\n  <button ion-button round (click)="CreateUser()">Create User</button>\n  <button ion-button round (click)="GetAllUser()">Display Users</button>\n  <button ion-button round (click)="DeleteAllUser()">Delete All</button>\n  <button ion-button round (click)="DeleteAllImages()">Delete Images</button>\n  <button ion-button round (click)="TakePhoto()">Take Photo</button>\n  \n</div>\n<h2>{{fileSavePath}}</h2>\n<img src="{{pathForImage(lastImage)}}" style="width: 100%" [hidden]="lastImage === null">\n<h3 [hidden]="lastImage !== null">Please Select Image!</h3>\n\n<img src="{{temp}}" style="width: 300px; height: 300px;">\n<ion-list>\n  <ion-item *ngFor="let dev of developers">\n    <h2>Id:{{dev.id}} Name:{{ dev.name }}</h2>\n    <p>Email:{{ dev.email }} </p>\n  </ion-item>\n</ion-list>\n</div>\n</ion-content>\n\n<ion-footer>\n    <ion-toolbar color="primary">\n      <ion-buttons>\n        <button ion-button icon-left (click)="presentActionSheet()">\n          <ion-icon name="camera"></ion-icon>Select Image\n        </button>\n        <button ion-button icon-left (click)="uploadImage()" [disabled]="lastImage === null">\n          <ion-icon name="cloud-upload"></ion-icon>Upload\n        </button>\n      </ion-buttons>\n    </ion-toolbar>\n  </ion-footer>\n'/*ion-inline-end:"/home/parth/parth/testApp3/src/pages/home/home.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_4__providers_database_database__["a" /* DatabaseProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* MenuController */],
            __WEBPACK_IMPORTED_MODULE_5__ionic_native_camera__["a" /* Camera */],
            __WEBPACK_IMPORTED_MODULE_7__ionic_native_transfer__["a" /* Transfer */],
            __WEBPACK_IMPORTED_MODULE_6__ionic_native_file__["a" /* File */],
            __WEBPACK_IMPORTED_MODULE_8__ionic_native_file_path__["a" /* FilePath */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* ActionSheetController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* ToastController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_9__providers_service_service__["a" /* ServiceProvider */]])
    ], HomePage);
    return HomePage;
}());

//# sourceMappingURL=home.js.map

/***/ }),

/***/ 205:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SignupPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__first_first__ = __webpack_require__(52);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_database_database__ = __webpack_require__(42);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_service_service__ = __webpack_require__(57);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var SignupPage = /** @class */ (function () {
    function SignupPage(navCtrl, navParams, alertCtrl, database, service) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.alertCtrl = alertCtrl;
        this.database = database;
        this.service = service;
        this.userDetail = [];
    }
    SignupPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad SignupPage');
    };
    SignupPage.prototype.gotofirst = function (name, email, user, pass) {
        var _this = this;
        console.log("Value received:" + name + email + user + pass);
        if (name.trim() == "" || email.trim() == "" || user.trim() == "" || pass.trim() == "") {
            var alert = this.alertCtrl.create({
                title: "Missing details",
                subTitle: "Fill all the fields"
            });
            console.log(name + email + user + pass);
            alert.present();
        }
        else {
            if (pass.length >= 6) {
                console.log("Strong password");
                var alert = this.alertCtrl.create({
                    title: 'Account Created',
                    subTitle: 'Now enjoy all the famous spots',
                    buttons: ['OK']
                });
                this.database.CreateUser(name, email, user, pass).then(function (data) {
                    console.log(data);
                }, function (error) {
                    console.log(error);
                });
                alert.present();
                this.database.getLastUser().then(function (data) {
                    _this.userDetail = data;
                    console.log("***********Service set:id " + _this.userDetail[0] + "  Name:" + _this.userDetail[1]);
                    _this.service.setService(_this.userDetail[0], _this.userDetail[1]);
                    console.log("Before setting root:" + _this.service.loginId + _this.service.loginName);
                    _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_2__first_first__["a" /* FirstPage */]);
                    _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__first_first__["a" /* FirstPage */], { data: user });
                }, function (error) {
                    console.log("Error while getting max value");
                });
            }
            else {
                console.log("Weak password");
                var alert = this.alertCtrl.create({
                    title: "Weak Password",
                    subTitle: "Length of the password should be atleast 6"
                });
                alert.present();
            }
        }
    };
    SignupPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-signup',template:/*ion-inline-start:"/home/parth/parth/testApp3/src/pages/signup/signup.html"*/'\n<ion-header>\n\n  <ion-navbar color="dark">\n    <ion-title>Signup</ion-title>\n    <!-- <button ion-button menuToggle end>\n      <ion-icon name="menu" ></ion-icon>\n    </button> -->\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n\n  <ion-list>\n    <ion-item>\n      <ion-label floating>Enter your name</ion-label>\n      <ion-input type="text" #name></ion-input>\n    </ion-item>\n    <ion-item>\n        <ion-label floating>Enter your email id</ion-label>\n        <ion-input type="text" #email></ion-input>\n      </ion-item>\n      <ion-item>\n          <ion-label floating>Enter your username</ion-label>\n          <ion-input type="text" #user></ion-input>\n        </ion-item>\n        <ion-item>\n            <ion-label floating>Enter your password</ion-label>\n            <ion-input type="password" #pass></ion-input>\n          </ion-item>\n  </ion-list>\n  <div >\n        <button ion-button  round (click)="gotofirst(name.value,email.value,user.value,pass.value)" id="signupbutton"> Create Account</button>\n  </div>\n</ion-content>\n'/*ion-inline-end:"/home/parth/parth/testApp3/src/pages/signup/signup.html"*/,
        }),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_3__providers_database_database__["a" /* DatabaseProvider */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__providers_database_database__["a" /* DatabaseProvider */]) === "function" && _d || Object, typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_4__providers_service_service__["a" /* ServiceProvider */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4__providers_service_service__["a" /* ServiceProvider */]) === "function" && _e || Object])
    ], SignupPage);
    return SignupPage;
    var _a, _b, _c, _d, _e;
}());

//# sourceMappingURL=signup.js.map

/***/ }),

/***/ 207:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AddSpotPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_camera__ = __webpack_require__(53);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_transfer__ = __webpack_require__(55);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_file_path__ = __webpack_require__(56);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_file__ = __webpack_require__(54);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_database_database__ = __webpack_require__(42);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









var AddSpotPage = /** @class */ (function () {
    function AddSpotPage(navCtrl, navParams, actionSheetCtrl, camera, platform, toastCtrl, loadingCtrl, transfer, file, filePath, dbProvider, alertCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.actionSheetCtrl = actionSheetCtrl;
        this.camera = camera;
        this.platform = platform;
        this.toastCtrl = toastCtrl;
        this.loadingCtrl = loadingCtrl;
        this.transfer = transfer;
        this.file = file;
        this.filePath = filePath;
        this.dbProvider = dbProvider;
        this.alertCtrl = alertCtrl;
        this.myDate = new Date().toISOString();
        this.description = "About the photo: Somethinng";
        this.dateT = "23 April,2018";
        this.imagename = "Picture ";
        this.fileSavePath = '';
    }
    AddSpotPage.prototype.saveData = function (name, description, dateT) {
        this.imagename = name;
        this.description = description;
        this.dateT = dateT;
        if (this.imagename.trim() == "" || this.description.trim() == "" || this.fileSavePath == "") {
            var alert_1 = this.alertCtrl.create({
                subTitle: 'Enter all the fields'
            });
            alert_1.present();
        }
        else {
            console.log(this.imagename + "  " + this.description + "  " + this.dateT);
            var alert_2 = this.alertCtrl.create({
                title: 'Voila! FSpot Added',
                buttons: [{
                        text: 'OK',
                        role: 'cancel'
                    }]
            });
            alert_2.present();
            this.copyFileToLocalDir(this.namePath, this.currentName, this.newFileName);
            this.navCtrl.pop();
        }
    };
    AddSpotPage.prototype.presentActionSheet = function () {
        var _this = this;
        var actionSheet = this.actionSheetCtrl.create({
            title: 'Select Image Source',
            buttons: [
                {
                    text: 'Use Camera',
                    handler: function () {
                        _this.takePicture(_this.camera.PictureSourceType.CAMERA);
                    }
                },
                {
                    text: 'Load from Library',
                    handler: function () {
                        _this.takePicture(_this.camera.PictureSourceType.PHOTOLIBRARY);
                    }
                },
                {
                    text: 'Cancel',
                    role: 'cancel'
                }
            ]
        });
        actionSheet.present();
    };
    AddSpotPage.prototype.takePicture = function (sourceType) {
        var _this = this;
        // Create options for the Camera Dialog
        var options = {
            quality: 60,
            sourceType: sourceType,
            saveToPhotoAlbum: false,
            correctOrientation: true,
            targetWidth: 2000,
            targetHeight: 1600,
            encodingType: this.camera.EncodingType.JPEG,
            destinationType: this.camera.DestinationType.FILE_URI
        };
        // Get the data of an image
        this.camera.getPicture(options).then(function (imagePath) {
            // Special handling for Android library
            //namePath, currentName, newFileName
            if (_this.platform.is('android') && sourceType === _this.camera.PictureSourceType.PHOTOLIBRARY) {
                _this.filePath.resolveNativePath(imagePath)
                    .then(function (filePath) {
                    _this.namePath = filePath.substr(0, filePath.lastIndexOf('/') + 1);
                    _this.currentName = imagePath.substring(imagePath.lastIndexOf('/') + 1, imagePath.lastIndexOf('?'));
                    _this.newFileName = _this.createFileName();
                    // this.copyFileToLocalDir(this.namePath, this.currentName,this.newFileName);
                    _this.fileSavePath = cordova.file.externalDataDirectory;
                    console.log("Image Path:" + _this.namePath + "  Name:" + _this.currentName);
                });
            }
            else {
                _this.currentName = imagePath.substr(imagePath.lastIndexOf('/') + 1);
                _this.namePath = imagePath.substr(0, imagePath.lastIndexOf('/') + 1);
                _this.newFileName = _this.createFileName();
                //this.copyFileToLocalDir(this.namePath, this.currentName, this.newFileName);
                console.log("Image Path:" + _this.namePath + "  Name:" + _this.currentName);
                _this.fileSavePath = cordova.file.externalDataDirectory;
            }
        }, function (err) {
            _this.presentToast('Error while selecting image');
        });
    };
    AddSpotPage.prototype.createFileName = function () {
        var d = new Date(), n = d.getTime(), newFileName = n + ".jpg";
        return newFileName;
    };
    // Copy the image to a local folder
    AddSpotPage.prototype.copyFileToLocalDir = function (namePath, currentName, newFileName) {
        var _this = this;
        console.log("Copying file to local ");
        this.file.copyFile(namePath, currentName, cordova.file.externalDataDirectory, newFileName).then(function (success) {
            _this.dbProvider.InsertImage(_this.imagename, _this.fileSavePath + newFileName, _this.description, _this.dateT);
        }, function (error) {
            _this.presentToast('Error while storing file.');
        });
    };
    AddSpotPage.prototype.presentToast = function (text) {
        var toast = this.toastCtrl.create({
            message: text,
            duration: 3000,
            position: 'top'
        });
        toast.present();
    };
    AddSpotPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad AddSpotPage');
    };
    AddSpotPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-add-spot',template:/*ion-inline-start:"/home/parth/parth/testApp3/src/pages/add-spot/add-spot.html"*/'\n<ion-header>\n\n  <ion-navbar color="dark">\n    <ion-title>AddSpot</ion-title>\n    <ion-buttons end>\n    <button  (click)="saveData(imagename.value,description.value,dateT)" id="addButton" >Add</button>\n  </ion-buttons>\n  </ion-navbar>\n  \n</ion-header>\n\n\n<ion-content padding>\n    <ion-list>\n        <ion-item>\n            <ion-label><ion-icon name="card" style="zoom:1.2;"></ion-icon> </ion-label>\n          <ion-textarea type="text"  placeholder="Name/Title/Place" style="font-size: 15px; height: 10px;" #imagename></ion-textarea>\n      </ion-item>\n      <ion-item>\n          <ion-label><ion-icon name="text" style="zoom:1.2;"></ion-icon></ion-label>\n          <ion-textarea placeholder="Describe in short" style="font-size: 15px;" #description></ion-textarea>\n          \n      </ion-item>\n      \n      <ion-item>\n        <ion-label><ion-icon name="calendar" style="zoom:1.2;"></ion-icon></ion-label>\n        <ion-datetime displayFormat="DD MMM, YYYY" style="padding-right: 170px" pickerFormat="DD MMM,YYYY" [(ngModel)]="myDate" #dateT ></ion-datetime>\n    </ion-item>\n    <ion-item>\n    <ion-label><ion-icon name="camera" style="zoom:1.2; color:grey"></ion-icon>\n      <button ion-button id="cameraButton" (click)="presentActionSheet()" clear style=" margin:0px 0px 0px 0px;  color: grey; width: 100%;height:30px;padding-bottom:15px; padding-right: 180px;">  {{ fileSavePath === \'\' ? \'Add a picture\' : fileSavePath }}</button>\n    </ion-label> \n    </ion-item>\n    <ion-item></ion-item>\n    </ion-list>\n</ion-content>\n'/*ion-inline-end:"/home/parth/parth/testApp3/src/pages/add-spot/add-spot.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* ActionSheetController */],
            __WEBPACK_IMPORTED_MODULE_2__ionic_native_camera__["a" /* Camera */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* ToastController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_native_transfer__["a" /* Transfer */],
            __WEBPACK_IMPORTED_MODULE_5__ionic_native_file__["a" /* File */],
            __WEBPACK_IMPORTED_MODULE_4__ionic_native_file_path__["a" /* FilePath */],
            __WEBPACK_IMPORTED_MODULE_6__providers_database_database__["a" /* DatabaseProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */]])
    ], AddSpotPage);
    return AddSpotPage;
}());

//# sourceMappingURL=add-spot.js.map

/***/ }),

/***/ 208:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(209);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_module__ = __webpack_require__(229);


Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_1__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 229:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__(27);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(201);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_status_bar__ = __webpack_require__(203);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__app_component__ = __webpack_require__(279);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__pages_home_home__ = __webpack_require__(204);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__pages_signup_signup__ = __webpack_require__(205);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__pages_first_first__ = __webpack_require__(52);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__pages_add_spot_add_spot__ = __webpack_require__(207);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__providers_database_database__ = __webpack_require__(42);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__ionic_native_sqlite__ = __webpack_require__(206);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__angular_http__ = __webpack_require__(285);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__angular_common_http__ = __webpack_require__(107);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__ionic_native_camera__ = __webpack_require__(53);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__ionic_native_file__ = __webpack_require__(54);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__ionic_native_transfer__ = __webpack_require__(55);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__ionic_native_file_path__ = __webpack_require__(56);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__providers_service_service__ = __webpack_require__(57);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



















var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_5__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_6__pages_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_7__pages_signup_signup__["a" /* SignupPage */],
                __WEBPACK_IMPORTED_MODULE_8__pages_first_first__["a" /* FirstPage */],
                __WEBPACK_IMPORTED_MODULE_9__pages_add_spot_add_spot__["a" /* AddSpotPage */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_12__angular_http__["a" /* HttpModule */],
                __WEBPACK_IMPORTED_MODULE_13__angular_common_http__["b" /* HttpClientModule */],
                __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["e" /* IonicModule */].forRoot(__WEBPACK_IMPORTED_MODULE_5__app_component__["a" /* MyApp */], {}, {
                    links: []
                })
            ],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["c" /* IonicApp */]],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_5__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_6__pages_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_7__pages_signup_signup__["a" /* SignupPage */],
                __WEBPACK_IMPORTED_MODULE_8__pages_first_first__["a" /* FirstPage */],
                __WEBPACK_IMPORTED_MODULE_9__pages_add_spot_add_spot__["a" /* AddSpotPage */]
            ],
            providers: [
                __WEBPACK_IMPORTED_MODULE_4__ionic_native_status_bar__["a" /* StatusBar */],
                __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */],
                __WEBPACK_IMPORTED_MODULE_11__ionic_native_sqlite__["a" /* SQLite */],
                __WEBPACK_IMPORTED_MODULE_15__ionic_native_file__["a" /* File */],
                __WEBPACK_IMPORTED_MODULE_16__ionic_native_transfer__["a" /* Transfer */],
                __WEBPACK_IMPORTED_MODULE_14__ionic_native_camera__["a" /* Camera */],
                __WEBPACK_IMPORTED_MODULE_17__ionic_native_file_path__["a" /* FilePath */],
                { provide: __WEBPACK_IMPORTED_MODULE_1__angular_core__["u" /* ErrorHandler */], useClass: __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["d" /* IonicErrorHandler */] },
                __WEBPACK_IMPORTED_MODULE_10__providers_database_database__["a" /* DatabaseProvider */],
                __WEBPACK_IMPORTED_MODULE_18__providers_service_service__["a" /* ServiceProvider */]
            ]
        })
    ], AppModule);
    return AppModule;
}());

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 279:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__ = __webpack_require__(203);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(201);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_home_home__ = __webpack_require__(204);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__pages_first_first__ = __webpack_require__(52);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__pages_my_spots_my_spots__ = __webpack_require__(286);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var MyApp = /** @class */ (function () {
    function MyApp(platform, statusBar, splashScreen, menuCtrl) {
        this.platform = platform;
        this.menuCtrl = menuCtrl;
        this.rootPage = __WEBPACK_IMPORTED_MODULE_4__pages_home_home__["a" /* HomePage */];
        platform.ready().then(function () {
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            statusBar.styleDefault();
            splashScreen.hide();
        });
    }
    MyApp.prototype.exitApplication = function () {
        this.platform.exitApp();
    };
    MyApp.prototype.goToHome = function () {
        console.log("Inside goToHome");
        this.menuCtrl.close();
        this.nav.setRoot(__WEBPACK_IMPORTED_MODULE_5__pages_first_first__["a" /* FirstPage */]);
    };
    MyApp.prototype.MyFavSpots = function () {
        this.menuCtrl.close();
        this.nav.setRoot(__WEBPACK_IMPORTED_MODULE_6__pages_my_spots_my_spots__["a" /* MySpotsPage */]);
    };
    MyApp.prototype.goToLogin = function () {
        console.log("Inside goToLogin");
        this.menuCtrl.close();
        this.nav.setRoot(__WEBPACK_IMPORTED_MODULE_4__pages_home_home__["a" /* HomePage */]);
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* Nav */]),
        __metadata("design:type", typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* Nav */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* Nav */]) === "function" && _a || Object)
    ], MyApp.prototype, "nav", void 0);
    MyApp = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({template:/*ion-inline-start:"/home/parth/parth/testApp3/src/app/app.html"*/'\n<!-- <ion-menu [content]="content" persistent="true" id="beforeLogin">\n        <ion-header>\n                \n              </ion-header>\n              <ion-content>\n                    <img src="../assets/imgs/moon.jpg" id="menuPic">\n                          \n                <ion-list>\n               <ion-item (click)="exitApplication()"><ion-icon name="exit"></ion-icon>&ensp;Exit</ion-item>\n\n                </ion-list>\n              </ion-content>\n</ion-menu> -->\n\n\n<ion-menu [content]="content" persistent="true" id="afterLogin">\n        <ion-header>\n                \n              </ion-header>\n              <ion-content>\n                    <img src="../assets/imgs/moon.jpg" id="menuPic">\n                          \n                <ion-list>\n                   \n                    <ion-item (click)="goToHome()" ><ion-icon name="home" ></ion-icon>&ensp;Home</ion-item>\n\n                   <ion-item><ion-icon name="bookmark" (click)="MyFavSpots()"></ion-icon>&ensp;My FavSpots</ion-item>\n                   <ion-item><ion-icon name="bookmark"></ion-icon>&ensp;Page 3</ion-item>     \n\n                   <ion-item (click)="goToLogin()">  <ion-icon name="log-out"></ion-icon>&ensp;Logout</ion-item>\n                   \n                   <ion-item (click)="exitApplication()">  <ion-icon name="exit"></ion-icon>&ensp;Exit</ion-item>\n                    \n               </ion-list>\n              </ion-content>\n</ion-menu>\n\n\n\n<ion-nav [root]="rootPage" #content></ion-nav>\n\n'/*ion-inline-end:"/home/parth/parth/testApp3/src/app/app.html"*/
        }),
        __metadata("design:paramtypes", [typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* Platform */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* Platform */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__["a" /* StatusBar */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__["a" /* StatusBar */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */]) === "function" && _d || Object, typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* MenuController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* MenuController */]) === "function" && _e || Object])
    ], MyApp);
    return MyApp;
    var _a, _b, _c, _d, _e;
}());

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 286:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MySpotsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_database_database__ = __webpack_require__(42);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var MySpotsPage = /** @class */ (function () {
    function MySpotsPage(navCtrl, navParams, dbProvider) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.dbProvider = dbProvider;
        this.images = [];
        this.getCurrentUserImages();
    }
    MySpotsPage.prototype.getCurrentUserImages = function () {
        var _this = this;
        this.dbProvider.getCurrentImage().then(function (data) {
            _this.images = data;
        }, function (error) {
        });
    };
    MySpotsPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad MySpotsPage');
    };
    MySpotsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-my-spots',template:/*ion-inline-start:"/home/parth/parth/testApp3/src/pages/my-spots/my-spots.html"*/'<ion-header>\n\n  <ion-navbar color="dark">\n    <ion-title>MySpots</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n  <ion-card *ngFor="let data of dataFromImageStore">\n    <div [ngStyle]="{\'background-image:url(\'+data.filelocation+\')\'}" style="background-size:cover;height: 220px;width: 100%;">\n\n    </div>\n  </ion-card>\n</ion-content>\n'/*ion-inline-end:"/home/parth/parth/testApp3/src/pages/my-spots/my-spots.html"*/,
        }),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_2__providers_database_database__["a" /* DatabaseProvider */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__providers_database_database__["a" /* DatabaseProvider */]) === "function" && _c || Object])
    ], MySpotsPage);
    return MySpotsPage;
    var _a, _b, _c;
}());

//# sourceMappingURL=my-spots.js.map

/***/ }),

/***/ 42:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DatabaseProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_common_http__ = __webpack_require__(107);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_sqlite__ = __webpack_require__(206);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var DatabaseProvider = /** @class */ (function () {
    function DatabaseProvider(http, storage) {
        var _this = this;
        this.http = http;
        this.storage = storage;
        if (!this.isOpen) {
            this.storage = new __WEBPACK_IMPORTED_MODULE_2__ionic_native_sqlite__["a" /* SQLite */]();
            this.storage.create({
                name: "data.db",
                location: "default"
            }).then(function (db) {
                _this.db = db;
                //this.db.executeSql("DROP TABLE imagestore",[])
                _this.db.executeSql("CREATE TABLE IF NOT EXISTS imagestore(id INTEGER primary key AUTOINCREMENT,name VARCHAR(300),filelocation varchar(300),description VARCHAR(1000), dateT VARCHAR(30))", {})
                    .then(function () { return console.log('Created table 2'); })
                    .catch(function (e) { return console.log(e); });
                //this.db.executeSql("DROP TABLE users",[])
                _this.db.executeSql("CREATE TABLE IF NOT EXISTS users(id INTEGER PRIMARY KEY AUTOINCREMENT, name VARCHAR(20),emailid VARCHAR(30),username VARCHAR(30),password VARCHAR(20))  ", {})
                    .then(function () { return console.log('Created table 1'); })
                    .catch(function (e) { return console.log(e); });
                _this.isOpen = true;
            }).catch(function (error) {
                console.log(error);
            });
        }
        /* if (!this.isOpen) {
           this.storage = new SQLite();
           this.storage.create({
             name: "data.db",
             location: "default"
           }).then((db: SQLiteObject) => {
             this.db = db;
     
             ////for table 1 and table 2
             // /*
             //   CREATE TABLE IF NOT EXISTS imagestore(
             //     id INTEGER primary key AUTOINCREMENT,
             //     name VARCHAR(70),
             //     description varchar(1000),
             //     dateT varchar(20))
             
             this.db.executeSql("CREATE TABLE IF NOT EXISTS imagestore(id INTEGER primary key AUTOINCREMENT,name VARCHAR(300),description varchar(1000), dateT varchar(30))", {})
               .then(() => console.log('Created table 2'))
               .catch(e => console.log(e));
             this.isOpen = true;
           }).catch((error) => {
             console.log(error);
           })
     
         }*/
    }
    DatabaseProvider.prototype.getCurrentImage = function () {
        return new Promise(function (resolve, reject) {
            var sql = "select * from imagestore";
        });
    };
    DatabaseProvider.prototype.InsertImage = function (name, filelocation, description, dateT) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var sql = "INSERT INTO imagestore(name,filelocation,description,dateT) VALUES(?,?,?,?)";
            _this.db.executeSql(sql, [name, filelocation, description, dateT]).then(function (data) {
                console.log("Date sent to insert:" + name + " " + filelocation + " " + description + " " + dateT);
                resolve(data);
            }, function (error) {
                reject(error);
            });
        });
    };
    DatabaseProvider.prototype.CreateUser = function (name, email, username, password) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var sql = "INSERT INTO users(name,emailid,username,password) values(?,?,?,?)";
            _this.db.executeSql(sql, [name, email, username, password]).then(function (data) {
                console.log("user insert success");
                resolve(data);
            }, function (error) {
                reject(error);
            });
        });
    };
    DatabaseProvider.prototype.getAllImage = function () {
        var _this = this;
        console.log("Inside getAllImages");
        return new Promise(function (resolve, reject) {
            var images = [];
            var sql = "select * from imagestore";
            _this.db.executeSql(sql, []).then(function (data) {
                if (data.rows.length > 0) {
                    for (var i = 0; i < data.rows.length; i++) {
                        //console.log("Name direct from db:"+data.rows.item(i).name);
                        images.push({
                            name: data.rows.item(i).name,
                            filelocation: data.rows.item(i).filelocation,
                            description: data.rows.item(i).description,
                            dateT: data.rows.item(i).dateT
                        });
                    }
                }
                // console.log("Just after pushing data in images:"+images);
                resolve(images);
            }, function (error) {
                reject(images);
            });
        });
    };
    DatabaseProvider.prototype.DeleteAllImages = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var sql = "delete from imagestore;";
            _this.db.executeSql(sql, []).then(function (data) {
                resolve(data);
                console.log("Data deleted");
            }, function (error) {
                reject(error);
            });
        });
    };
    DatabaseProvider.prototype.DeleteAllUser = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var sql = "delete from USERS";
            var sql1 = "UPDATE SQLITE_SEQUENCE SET SEQ=0 WHERE NAME='users';";
            _this.db.executeSql(sql1, []).then(function (data) {
                resolve(data);
            }, function (error) {
                reject(error);
            });
            _this.db.executeSql(sql, []).then(function (data) {
                console.log("Data deleted");
                resolve(data);
            }, function (error) {
                reject(error);
            });
        });
    };
    DatabaseProvider.prototype.getUser = function (name, pass) {
        var _this = this;
        console.log("Check name:" + name + pass);
        return new Promise(function (resolve, reject) {
            var sql = "select * from users where name=? and password=?";
            _this.db.executeSql(sql, [name, pass]).then(function (data) {
                console.log("Query executed");
                if (data.rows.length == 1)
                    resolve(data.rows.item(0).id);
                else
                    resolve(-1);
            }, function (error) {
                reject(error);
            });
        });
    };
    DatabaseProvider.prototype.getLastUser = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var arr;
            var sql = "select * from users order by id desc limit 1;";
            _this.db.executeSql(sql, []).then(function (data) {
                console.log("*** inside getLastUser of database.ts:Id :" + data.rows.item(0).id + "  Name:" + data.rows.item(0).username);
                arr = [data.rows.item(0).id, data.rows.item(0).username];
                //arr[0]=data.rows.item(0).username;
                console.log("*** inside getLastUser of database.ts:" + arr[0] + " " + arr[1]);
                resolve(arr);
            }, function (error) { return reject(error); });
        });
    };
    DatabaseProvider.prototype.GetAllUsers = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var developers = [];
            var sql = "SELECT * FROM users";
            _this.db.executeSql(sql, []).then(function (data) {
                if (data.rows.length > 0) {
                    for (var i = 0; i < data.rows.length; i++) {
                        developers.push({
                            id: data.rows.item(i).id,
                            name: data.rows.item(i).name,
                            email: data.rows.item(i).emailid
                        });
                    }
                }
                resolve(developers);
            }, function (error) {
                reject(developers);
            });
        });
    };
    DatabaseProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_0__angular_common_http__["a" /* HttpClient */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_common_http__["a" /* HttpClient */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__ionic_native_sqlite__["a" /* SQLite */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__ionic_native_sqlite__["a" /* SQLite */]) === "function" && _b || Object])
    ], DatabaseProvider);
    return DatabaseProvider;
    var _a, _b;
}());

//# sourceMappingURL=database.js.map

/***/ }),

/***/ 52:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FirstPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_camera__ = __webpack_require__(53);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_file__ = __webpack_require__(54);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_transfer__ = __webpack_require__(55);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_file_path__ = __webpack_require__(56);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_database_database__ = __webpack_require__(42);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__add_spot_add_spot__ = __webpack_require__(207);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__providers_service_service__ = __webpack_require__(57);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};










var FirstPage = /** @class */ (function () {
    function FirstPage(navCtrl, navParams, platform, menuCtrl, camera, transfer, file, filePath, dbProvider, alertCtrl, toastCtrl, loadingCtrl, service) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.platform = platform;
        this.menuCtrl = menuCtrl;
        this.camera = camera;
        this.transfer = transfer;
        this.file = file;
        this.filePath = filePath;
        this.dbProvider = dbProvider;
        this.alertCtrl = alertCtrl;
        this.toastCtrl = toastCtrl;
        this.loadingCtrl = loadingCtrl;
        this.service = service;
        this.lastImage = null;
        //temp="file:///storage/sdcard0/Android/data/io.ionic.starter/files/a.jpg";
        this.description = "About the photo: Something";
        this.dateT = "23 April,2018";
        this.imagename = "Picture ";
        this.dataFromImageStore = [];
        this.nameService = this.service.getServiceName();
        this.idService = this.service.getServiceId();
        this.name = navParams.get('data');
        this.getImagefromImageStore();
        ////for 2 types of menus
        // this.menuCtrl.enable(false,"beforeLogin");
        // this.menuCtrl.enable(true,"afterLogin");      //2 menus were created .(check app.html)
        platform.registerBackButtonAction(function () {
            platform.exitApp();
        });
    }
    FirstPage.prototype.getAddPage = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_7__add_spot_add_spot__["a" /* AddSpotPage */]);
    };
    FirstPage.prototype.getImagefromImageStore = function () {
        var _this = this;
        console.log("Inside getImagefromImageStore in first.ts");
        this.dbProvider.getAllImage().then(function (data) {
            _this.dataFromImageStore = data;
            console.log(data);
        }, function (error) {
            console.log(error);
        });
    };
    FirstPage.prototype.TakePhotoFromCamera = function () {
        this.takePicture(this.camera.PictureSourceType.CAMERA);
    };
    FirstPage.prototype.TakePhotoFromGallery = function () {
        this.takePicture(this.camera.PictureSourceType.PHOTOLIBRARY);
    };
    FirstPage.prototype.takePicture = function (sourceType) {
        var _this = this;
        // Create options for the Camera Dialog
        var options = {
            quality: 100,
            sourceType: sourceType,
            saveToPhotoAlbum: false,
            correctOrientation: true,
            targetWidth: 1000,
            targetHeight: 800,
            encodingType: this.camera.EncodingType.JPEG,
            destinationType: this.camera.DestinationType.FILE_URI,
            allowEdit: true
        };
        // Get the data of an image
        this.camera.getPicture(options).then(function (imagePath) {
            // Special handling for Android library
            if (_this.platform.is('android') && sourceType === _this.camera.PictureSourceType.PHOTOLIBRARY) {
                _this.filePath.resolveNativePath(imagePath)
                    .then(function (filePath) {
                    var correctPath = filePath.substr(0, filePath.lastIndexOf('/') + 1);
                    var currentName = imagePath.substring(imagePath.lastIndexOf('/') + 1, imagePath.lastIndexOf('?'));
                    _this.copyFileToLocalDir(correctPath, currentName, _this.createFileName());
                    _this.fileSavePath = cordova.file.externalDataDirectory;
                    console.log("Image Path:" + correctPath + "  Name:" + currentName);
                });
            }
            else {
                var currentName = imagePath.substr(imagePath.lastIndexOf('/') + 1);
                var correctPath = imagePath.substr(0, imagePath.lastIndexOf('/') + 1);
                _this.copyFileToLocalDir(correctPath, currentName, _this.createFileName());
                console.log("Image Path:" + correctPath + "  Name:" + currentName);
                _this.fileSavePath = cordova.file.externalDataDirectory;
            }
        }, function (err) {
            _this.presentToast('Error while selecting image.');
        });
    };
    FirstPage.prototype.createFileName = function () {
        var d = new Date(), n = d.getTime(), newFileName = n + ".jpg";
        return newFileName;
    };
    // Copy the image to a local folder
    FirstPage.prototype.copyFileToLocalDir = function (namePath, currentName, newFileName) {
        var _this = this;
        console.log("Copying file to local ");
        this.file.copyFile(namePath, currentName, cordova.file.externalDataDirectory, newFileName).then(function (success) {
            _this.dbProvider.InsertImage(_this.imagename, _this.fileSavePath + newFileName, _this.description, _this.dateT);
            //this.lastImage = newFileName;
            //this.fileSavePath=cordova.file.dataDirectory;
            // this.presentToast(' file Stored in.'+cordova.file.dataDirectory);
        }, function (error) {
            _this.presentToast('Error while storing file.');
        });
    };
    FirstPage.prototype.presentToast = function (text) {
        var toast = this.toastCtrl.create({
            message: text,
            duration: 3000,
            position: 'top'
        });
        toast.present();
    };
    FirstPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad FirstPage');
    };
    FirstPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-first',template:/*ion-inline-start:"/home/parth/parth/testApp3/src/pages/first/first.html"*/'\n<ion-header>\n  <ion-navbar hideBackButton color="dark">\n    \n    <ion-title>FavSpot</ion-title>\n    <button ion-button menuToggle end>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n\n    \n  </ion-navbar>\n\n</ion-header>\n\n<ion-content padding>\n\n<p>Hello {{name}}</p>\n<br>From Service Name:{{nameService}}  Id:{{idService}}\n\n<ion-fab right bottom>\n  <button ion-fab  id="fabButton" (click)="getAddPage()"><ion-icon name="add"></ion-icon></button>\n  <!-- <ion-fab-list side="left">\n    <ion-icon name="camera" id="iconSpace" style="zoom:2.0;" (click)="TakePhotoFromCamera()"></ion-icon>\n    <ion-icon name="image" id="iconSpace" style="zoom:2.0;" (click)="TakePhotoFromGallery()"></ion-icon>\n  </ion-fab-list> -->\n</ion-fab>\n  \n\n<!-- background: url(\'{{data.filelocation}}\') no-repeat -->\n  <ion-card *ngFor="let data of dataFromImageStore">\n    <div [ngStyle]="{\'background-image\': \'url(\'+data.filelocation+\')\'}" style=" background-size: cover;height: 220px;width:100%;\n    background-size: cover;background-position: center center;">\n   </div>\n     <ion-title> \n       {{data.name}}\n     </ion-title> \n  <ion-card-content >\n     <p>{{data.description}}</p> \n    </ion-card-content> \n  </ion-card> \n  \n\n\n   \n\n</ion-content>\n'/*ion-inline-end:"/home/parth/parth/testApp3/src/pages/first/first.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* MenuController */],
            __WEBPACK_IMPORTED_MODULE_2__ionic_native_camera__["a" /* Camera */],
            __WEBPACK_IMPORTED_MODULE_4__ionic_native_transfer__["a" /* Transfer */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_native_file__["a" /* File */],
            __WEBPACK_IMPORTED_MODULE_5__ionic_native_file_path__["a" /* FilePath */],
            __WEBPACK_IMPORTED_MODULE_6__providers_database_database__["a" /* DatabaseProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* ToastController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_8__providers_service_service__["a" /* ServiceProvider */]])
    ], FirstPage);
    return FirstPage;
}());

//# sourceMappingURL=first.js.map

/***/ }),

/***/ 57:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ServiceProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_common_http__ = __webpack_require__(107);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ServiceProvider = /** @class */ (function () {
    function ServiceProvider(http) {
        this.http = http;
        console.log('Hello ServiceProvider Provider');
        this.loginId = 0;
        this.loginName = "";
    }
    ServiceProvider.prototype.setService = function (id, name) {
        console.log("***Inside serviceProvider : Id:" + id + "  Name:" + name);
        this.loginId = id;
        this.loginName = name;
    };
    ServiceProvider.prototype.getServiceName = function () {
        return this.loginName;
    };
    ServiceProvider.prototype.getServiceId = function () {
        return this.loginId;
    };
    ServiceProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_0__angular_common_http__["a" /* HttpClient */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_common_http__["a" /* HttpClient */]) === "function" && _a || Object])
    ], ServiceProvider);
    return ServiceProvider;
    var _a;
}());

//# sourceMappingURL=service.js.map

/***/ })

},[208]);
//# sourceMappingURL=main.js.map