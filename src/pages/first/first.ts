import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform, MenuController, Loading, ToastController, LoadingController } from 'ionic-angular';


import { Camera, CameraOptions } from '@ionic-native/camera';
import { File } from '@ionic-native/file';
import { Transfer, TransferObject } from '@ionic-native/transfer';
import { FilePath } from '@ionic-native/file-path';
import {DatabaseProvider} from '../../providers/database/database';
import { AlertController } from 'ionic-angular';
import {AddSpotPage} from '../add-spot/add-spot';
import {ServiceProvider} from '../../providers/service/service'

declare var cordova: any;

@Component({
  selector: 'page-first',
  templateUrl: 'first.html',
})
export class FirstPage {

  fileSavePath :string;
  name: string;
  lastImage: string = null;
  loading: Loading;
  nameService:string;
  idService:any;
 

  //temp="file:///storage/sdcard0/Android/data/io.ionic.starter/files/a.jpg";
  description="About the photo: Something";
  dateT="23 April,2018";
  imagename="Picture ";

  dataFromImageStore:any= [];

  constructor(public navCtrl: NavController, 
    public navParams: NavParams,
     private platform:Platform,
    private menuCtrl: MenuController,
    private camera: Camera,
    private transfer: Transfer,
    private file: File,
    private filePath: FilePath,
    private dbProvider:DatabaseProvider,
    private alertCtrl:AlertController,
    public toastCtrl: ToastController,
    public loadingCtrl: LoadingController,
    private service:ServiceProvider) {

   
    this.nameService=this.service.getServiceName();
    this.idService=this.service.getServiceId();
    this.name=navParams.get('data');

    this.getImagefromImageStore();
      

    ////for 2 types of menus
    // this.menuCtrl.enable(false,"beforeLogin");
    // this.menuCtrl.enable(true,"afterLogin");      //2 menus were created .(check app.html)

    platform.registerBackButtonAction(()=>{
      
      platform.exitApp();
    })
    
  }

  getAddPage(){
    this.navCtrl.push(AddSpotPage);
  }


  getImagefromImageStore(){
    console.log("Inside getImagefromImageStore in first.ts");
      this.dbProvider.getAllImage().then((data)=>{
        this.dataFromImageStore=data;
        console.log(data);
      },(error)=>{
        console.log(error);
      })
  }


  TakePhotoFromCamera(){
    this.takePicture(this.camera.PictureSourceType.CAMERA);

  }

  TakePhotoFromGallery(){
    this.takePicture(this.camera.PictureSourceType.PHOTOLIBRARY);

  }

  public takePicture(sourceType) {
 
    // Create options for the Camera Dialog
    var options = {
      quality: 100,
      sourceType: sourceType,
      saveToPhotoAlbum: false,
      correctOrientation: true,
      targetWidth:1000,
      targetHeight:800,
      encodingType:this.camera.EncodingType.JPEG,
      destinationType: this.camera.DestinationType.FILE_URI,
      allowEdit:true
    };

    // Get the data of an image
    this.camera.getPicture(options).then((imagePath) => {
      // Special handling for Android library
      if (this.platform.is('android') && sourceType === this.camera.PictureSourceType.PHOTOLIBRARY) {
        this.filePath.resolveNativePath(imagePath)
          .then(filePath => {
            let correctPath = filePath.substr(0, filePath.lastIndexOf('/') + 1);
            let currentName = imagePath.substring(imagePath.lastIndexOf('/') + 1, imagePath.lastIndexOf('?'));
            this.copyFileToLocalDir(correctPath, currentName, this.createFileName());
            this.fileSavePath=cordova.file.externalDataDirectory;
            console.log("Image Path:"+correctPath+ "  Name:"+currentName);
          });
      } else {
        var currentName = imagePath.substr(imagePath.lastIndexOf('/') + 1);
        var correctPath = imagePath.substr(0, imagePath.lastIndexOf('/') + 1);
        this.copyFileToLocalDir(correctPath, currentName, this.createFileName());
        console.log("Image Path:"+correctPath+ "  Name:"+currentName);
        this.fileSavePath=cordova.file.externalDataDirectory;
      }
      
    }, (err) => {
      this.presentToast('Error while selecting image.');
    });

    
  }

  private createFileName() {
    var d = new Date(),
      n = d.getTime(),
      newFileName = n + ".jpg";
    return newFileName;
  }

  // Copy the image to a local folder
  private copyFileToLocalDir(namePath, currentName, newFileName) {
    console.log("Copying file to local ");
    this.file.copyFile(namePath, currentName, cordova.file.externalDataDirectory, newFileName).then(success => {
      
      this.dbProvider.InsertImage(this.imagename,this.fileSavePath+newFileName,this.description,this.dateT);
      //this.lastImage = newFileName;
      //this.fileSavePath=cordova.file.dataDirectory;
     // this.presentToast(' file Stored in.'+cordova.file.dataDirectory);
    }, error => {
      this.presentToast('Error while storing file.');
    });
  }

  private presentToast(text) {
    let toast = this.toastCtrl.create({
      message: text,
      duration: 3000,
      position: 'top'
    });
    toast.present();
  }



  ionViewDidLoad() {
    console.log('ionViewDidLoad FirstPage');
  }


}
