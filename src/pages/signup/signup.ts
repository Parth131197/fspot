import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Button } from 'ionic-angular';
import { FirstPage } from '../first/first';
import { AlertController } from 'ionic-angular';
import {DatabaseProvider} from '../../providers/database/database';
import {ServiceProvider} from '../../providers/service/service'
@Component({
  selector: 'page-signup',
  templateUrl: 'signup.html',
})
export class SignupPage {

   userDetail: any=[];
  
  constructor(public navCtrl: NavController, public navParams: NavParams,
    public alertCtrl:AlertController,
    private database:DatabaseProvider,
    private service:ServiceProvider) {

      
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SignupPage');
  }

  gotofirst(name,email,user,pass){
    console.log("Value received:"+name+email+user+pass);
    if(name.trim()==""||email.trim()==""||user.trim()==""||pass.trim()==""){
      
      const alert=this.alertCtrl.create({
        title:"Missing details",
        subTitle:"Fill all the fields"
      });
      console.log(name+email+user+pass);
      alert.present();
    }
    else{

      if(pass.length>=6){
          console.log("Strong password");
          const alert=this.alertCtrl.create({
            title:'Account Created',  
            subTitle:'Now enjoy all the famous spots',
            buttons:['OK']
        });
        this.database.CreateUser(name,email,user,pass).then((data) => {
            console.log(data);
          }, (error) => {
            console.log(error);
          })
        alert.present();
        
        this.database.getLastUser().then((data)=>{
            this.userDetail=data;
            console.log("***********Service set:id "+this.userDetail[0]+"  Name:"+this.userDetail[1]);

            this.service.setService(this.userDetail[0],this.userDetail[1]);
            console.log("Before setting root:"+this.service.loginId+this.service.loginName);
            this.navCtrl.setRoot(FirstPage);   
            this.navCtrl.push(FirstPage,{data:user});

        },(error)=>{
          console.log("Error while getting max value");
        })
       
          

        } 
      else{
        console.log("Weak password");
        const alert=this.alertCtrl.create({
          title:"Weak Password",
          subTitle:"Length of the password should be atleast 6"
        });
        alert.present();
      } 
      
    }
    
  }
}
