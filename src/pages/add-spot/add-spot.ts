import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,Platform, ToastController, LoadingController } from 'ionic-angular';
import {ActionSheetController} from 'ionic-angular';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { Transfer } from '@ionic-native/transfer';
import { FilePath } from '@ionic-native/file-path';
import { File } from '@ionic-native/file';
import{DatabaseProvider} from '../../providers/database/database'
import {AlertController} from 'ionic-angular'

declare var cordova: any;


@Component({
  selector: 'page-add-spot',
  templateUrl: 'add-spot.html',
})
export class AddSpotPage {

  myDate:any  = new Date().toISOString();
  fileSavePath :string;
  description="About the photo: Somethinng";
  dateT="23 April,2018";
  imagename="Picture ";
  namePath:string; currentName:string; newFileName:string;

  constructor(public navCtrl: NavController, public navParams: NavParams,
  private actionSheetCtrl:ActionSheetController,
  private camera:Camera,
  private platform:Platform,
  public toastCtrl: ToastController,
  public loadingCtrl: LoadingController,
  private transfer: Transfer,
  private file: File,
  private filePath: FilePath,
  private dbProvider: DatabaseProvider,
  private alertCtrl: AlertController) {

    this.fileSavePath='';

  }


  saveData(name,description,dateT){
    this.imagename=name;
    this.description=description;
    this.dateT=dateT;

    if(this.imagename.trim()==""||this.description.trim()==""||this.fileSavePath=="")
    {
      const alert=this.alertCtrl.create({
        subTitle:'Enter all the fields'
      });
      alert.present();
    }else{
      console.log(this.imagename+"  "+this.description+"  "+this.dateT);
      const alert=this.alertCtrl.create({
        title:'Voila! FSpot Added',
        buttons:[{
          text:'OK',
          role:'cancel'
        }]
      });
      alert.present();
      this.copyFileToLocalDir(this.namePath, this.currentName, this.newFileName);
      this.navCtrl.pop();
    }
    
  }

  public presentActionSheet() {
    let actionSheet = this.actionSheetCtrl.create({
      title: 'Select Image Source',
      buttons: [
        {
          text: 'Use Camera',
          handler: () => {
            this.takePicture(this.camera.PictureSourceType.CAMERA);
          }
        },
        {
          text: 'Load from Library',
          handler: () => {
            this.takePicture(this.camera.PictureSourceType.PHOTOLIBRARY);
          }
        },
        {
          text: 'Cancel',
          role: 'cancel'
        }
      ]
    });
    actionSheet.present();

  }


  public takePicture(sourceType) {
    // Create options for the Camera Dialog
    var options = {
      quality: 60,
      sourceType: sourceType,
      saveToPhotoAlbum: false,
      correctOrientation: true,
      targetWidth:2000,
      targetHeight:1600,
      encodingType:this.camera.EncodingType.JPEG,
      destinationType: this.camera.DestinationType.FILE_URI
    };

    // Get the data of an image
    this.camera.getPicture(options).then((imagePath) => {
      // Special handling for Android library
      //namePath, currentName, newFileName
      if (this.platform.is('android') && sourceType === this.camera.PictureSourceType.PHOTOLIBRARY) {
        this.filePath.resolveNativePath(imagePath)
          .then(filePath => {
            this.namePath = filePath.substr(0, filePath.lastIndexOf('/') + 1);
            this.currentName = imagePath.substring(imagePath.lastIndexOf('/') + 1, imagePath.lastIndexOf('?'));
            this.newFileName=this.createFileName();
            
           // this.copyFileToLocalDir(this.namePath, this.currentName,this.newFileName);

            this.fileSavePath=cordova.file.externalDataDirectory;
            console.log("Image Path:"+this.namePath+ "  Name:"+this.currentName);
          });
      } else {
        this.currentName = imagePath.substr(imagePath.lastIndexOf('/') + 1);
        this.namePath = imagePath.substr(0, imagePath.lastIndexOf('/') + 1);
        this.newFileName=this.createFileName();
        //this.copyFileToLocalDir(this.namePath, this.currentName, this.newFileName);
        console.log("Image Path:"+this.namePath+ "  Name:"+this.currentName);
        this.fileSavePath=cordova.file.externalDataDirectory;
      }
      
    }, (err) => {
      this.presentToast('Error while selecting image');
    });

    
  }

  private createFileName() {
    var d = new Date(),
      n = d.getTime(),
      newFileName = n + ".jpg";
    return newFileName;
  }

  // Copy the image to a local folder
  
  private copyFileToLocalDir(namePath, currentName, newFileName) {
    console.log("Copying file to local ");
    this.file.copyFile(namePath, currentName, cordova.file.externalDataDirectory, newFileName).then(success => {
      
      this.dbProvider.InsertImage(this.imagename,this.fileSavePath+newFileName,this.description,this.dateT);
      
    }, error => {
      this.presentToast('Error while storing file.');
    });
  }

  private presentToast(text) {
    let toast = this.toastCtrl.create({
      message: text,
      duration: 3000,
      position: 'top'
    });
    toast.present();
  }




  ionViewDidLoad() {
    console.log('ionViewDidLoad AddSpotPage');
  }

}
