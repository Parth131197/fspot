import { Component } from '@angular/core';
import { NavController, MenuController, Loading, ActionSheetController, ToastController, Platform, LoadingController } from 'ionic-angular';
import { SignupPage } from '../signup/signup';
import { FirstPage } from '../first/first';
import { DatabaseProvider } from '../../providers/database/database';
import { AlertController } from 'ionic-angular';



import { Camera, CameraOptions } from '@ionic-native/camera';
import { File } from '@ionic-native/file';
import { Transfer, TransferObject } from '@ionic-native/transfer';
import { FilePath } from '@ionic-native/file-path';
import {ServiceProvider} from '../../providers/service/service'

declare var cordova: any;

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})



export class HomePage {

  fileSavePath :string;
  splash = true;

  developers: any = [];
  developer: any = {};

  lastImage: string = null;
  loading: Loading;
  temp="file:///storage/sdcard0/Android/data/io.ionic.starter/files/a.jpg";


  constructor(public navCtrl: NavController,
    private database: DatabaseProvider,
    private alertCtrl: AlertController,
    private menuCtrl: MenuController,
    private camera: Camera,
    private transfer: Transfer,
    private file: File,
    private filePath: FilePath,
    public actionSheetCtrl: ActionSheetController,
    public toastCtrl: ToastController,
    public platform: Platform,
    public loadingCtrl: LoadingController,
    private service:ServiceProvider) {

    //for 2 types of menus
    // this.menuCtrl.enable(true,"beforeLogin");  
    // this.menuCtrl.enable(false,"afterLogin");


    this.GetAllUser();
  }

  ////////////////////////////////////////////////////////////////////////
  public presentActionSheet() {
    let actionSheet = this.actionSheetCtrl.create({
      title: 'Select Image Source',
      buttons: [
        {
          text: 'Load from Library',
          handler: () => {
            this.takePicture(this.camera.PictureSourceType.PHOTOLIBRARY);
          }
        },
        {
          text: 'Use Camera',
          handler: () => {
            this.takePicture(this.camera.PictureSourceType.CAMERA);
          }
        },
        {
          text: 'Cancel',
          role: 'cancel'
        }
      ]
    });
    actionSheet.present();

  }

  public takePicture(sourceType) {
    // Create options for the Camera Dialog
    var options = {
      quality: 100,
      sourceType: sourceType,
      saveToPhotoAlbum: false,
      correctOrientation: true,
      targetWidth:400,
      targetHeight:400,
      destinationType: this.camera.DestinationType.FILE_URI
    };

    // Get the data of an image
    this.camera.getPicture(options).then((imagePath) => {
      // Special handling for Android library
      if (this.platform.is('android') && sourceType === this.camera.PictureSourceType.PHOTOLIBRARY) {
        this.filePath.resolveNativePath(imagePath)
          .then(filePath => {
            let correctPath = filePath.substr(0, filePath.lastIndexOf('/') + 1);
            let currentName = imagePath.substring(imagePath.lastIndexOf('/') + 1, imagePath.lastIndexOf('?'));
            this.copyFileToLocalDir(correctPath, currentName, this.createFileName());
            this.fileSavePath=cordova.file.externalDataDirectory;
            console.log("Image Path:"+correctPath+ "  Name:"+currentName);
          });
      } else {
        var currentName = imagePath.substr(imagePath.lastIndexOf('/') + 1);
        var correctPath = imagePath.substr(0, imagePath.lastIndexOf('/') + 1);
        this.copyFileToLocalDir(correctPath, currentName, this.createFileName());
        console.log("Image Path:"+correctPath+ "  Name:"+currentName);
        this.fileSavePath=cordova.file.externalDataDirectory;
      }
      
    }, (err) => {
      this.presentToast('Error while selecting image.');
    });

    
  }

  private createFileName() {
    var d = new Date(),
      n = d.getTime(),
      newFileName = n + ".jpg";
    return newFileName;
  }

  // Copy the image to a local folder
  private copyFileToLocalDir(namePath, currentName, newFileName) {
    console.log("Copying file to local ");
    this.file.copyFile(namePath, currentName, cordova.file.externalDataDirectory, newFileName).then(success => {
      //this.lastImage = newFileName;
      //this.fileSavePath=cordova.file.dataDirectory;
     // this.presentToast(' file Stored in.'+cordova.file.dataDirectory);
    }, error => {
      this.presentToast('Error while storing file.');
    });
  }

  private presentToast(text) {
    let toast = this.toastCtrl.create({
      message: text,
      duration: 3000,
      position: 'top'
    });
    toast.present();
  }

  // Always get the accurate path to your apps folder
  public pathForImage(img) {
    if (img === null) {
      return '';
    } else {
      return cordova.file.dataDirectory + img;
    }
  }

  ////////////////////////////////////////////////////////









  ionViewDidLoad() {
    setTimeout(() => {
      this.splash = false;
    }, 3000)
  }


  TakePhoto() {
   /*
    this.camera.getPicture({
  destinationType: this.camera.DestinationType.FILE_URI,
  sourceType: sourceType,
  mediaType: this.camera.MediaType.PICTURE,
  encodingType: this.camera.EncodingType.JPEG,
  saveToPhotoAlbum: (source === PictureSource.CAMERA),
  allowEdit: true
})
  .then(imageUri => {
    this.imageResizer.resize({
      uri: imageUri,
      quality: 60,
      width: 1280,
      height: 1280
    }).then(uri => handler(uri))
  })
  .catch(error => console.warn(error))
}


    */


    const options: CameraOptions = {
      quality: 100,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE
    }

    this.camera.getPicture(options).then((imageData) => {
      // imageData is either a base64 encoded string or a file URI
      // If it's base64:
      let base64Image = 'data:image/jpeg;base64,' + imageData;
    }, (err) => {
      // Handle error
    });
  }

  DeleteAllUser() {
    this.database.DeleteAllUser();
  }

  DeleteAllImages(){
    this.database.DeleteAllImages();
  }
  CreateUser() {
    this.database.CreateUser("Parth", "parth@parth.com", "kevin", "123456").then((data) => {
      console.log(data);
    }, (error) => {
      console.log(error);
    })
  }

  GetAllUser() {
    this.database.GetAllUsers().then((data) => {
      this.developers = data;
      console.log(data);
    }, (error) => {
      console.log(error);
    })
  }

  signup() {
    this.navCtrl.push(SignupPage);
  }

  login(name, pass) {

    console.log("Name:" + name + " Pass:" + pass);

    this.database.getUser(name, pass).then((data) => {
      console.log(data);
      if (data > 0){
        this.service.setService(data,name);
        
        this.navCtrl.push(FirstPage, { data: name });
      }
      else {
        const alert = this.alertCtrl.create({
          subTitle: "Invalid username or password"
        });
        alert.present();
      }
    }, (error) => {
      console.log(error);
    })



  }

}
