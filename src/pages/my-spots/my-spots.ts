import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {DatabaseProvider} from '../../providers/database/database'


@Component({
  selector: 'page-my-spots',
  templateUrl: 'my-spots.html',
})
export class MySpotsPage {

   images:any=[];
  constructor(public navCtrl: NavController, public navParams: NavParams,
  private dbProvider:DatabaseProvider) {
    this.getCurrentUserImages();

  }

  getCurrentUserImages(){
      this.dbProvider.getCurrentImage().then((data)=>{
        this.images=data;
      },(error)=>{

      })
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MySpotsPage');
  }

}
