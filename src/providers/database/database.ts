import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';

@Injectable()
export class DatabaseProvider {
  
  private db: SQLiteObject;
  private isOpen: boolean;
  constructor(public http: HttpClient, public storage: SQLite) {

    if (!this.isOpen) {
      this.storage = new SQLite();
      this.storage.create({
        name: "data.db",
        location: "default"
      }).then((db: SQLiteObject) => {
        this.db = db;
        //this.db.executeSql("DROP TABLE imagestore",[])
         this.db.executeSql("CREATE TABLE IF NOT EXISTS imagestore(id INTEGER primary key AUTOINCREMENT,name VARCHAR(300),filelocation varchar(300),description VARCHAR(1000), dateT VARCHAR(30))", {})
          .then(() => console.log('Created table 2'))
          .catch(e => console.log(e));
        
        //this.db.executeSql("DROP TABLE users",[])
        this.db.executeSql("CREATE TABLE IF NOT EXISTS users(id INTEGER PRIMARY KEY AUTOINCREMENT, name VARCHAR(20),emailid VARCHAR(30),username VARCHAR(30),password VARCHAR(20))  ", {})
          .then(() => console.log('Created table 1'))
          .catch(e => console.log(e));


        this.isOpen = true;
      }).catch((error) => {
        console.log(error);
      })

    }

   /* if (!this.isOpen) {
      this.storage = new SQLite();
      this.storage.create({
        name: "data.db",
        location: "default"
      }).then((db: SQLiteObject) => {
        this.db = db;

        ////for table 1 and table 2
        // /*
        //   CREATE TABLE IF NOT EXISTS imagestore(
        //     id INTEGER primary key AUTOINCREMENT,
        //     name VARCHAR(70),
        //     description varchar(1000), 
        //     dateT varchar(20))
        
        this.db.executeSql("CREATE TABLE IF NOT EXISTS imagestore(id INTEGER primary key AUTOINCREMENT,name VARCHAR(300),description varchar(1000), dateT varchar(30))", {})
          .then(() => console.log('Created table 2'))
          .catch(e => console.log(e));
        this.isOpen = true;
      }).catch((error) => {
        console.log(error);
      })

    }*/

  }

  getCurrentImage(){
    return new Promise((resolve,reject)=>{
      let sql="select * from imagestore";
    })
  }


  InsertImage(name,filelocation,description,dateT){
    return new Promise((resolve,reject)=>{
      let sql="INSERT INTO imagestore(name,filelocation,description,dateT) VALUES(?,?,?,?)";
      this.db.executeSql(sql,[name,filelocation,description,dateT]).then((data)=>{
        console.log("Date sent to insert:"+name+" "+filelocation+" "+description+" "+dateT);
        resolve(data);
      },(error)=>{
        reject(error);
      })
    })
  }

  CreateUser(name: string, email: string,username:string,password:string) {
    return new Promise((resolve, reject) => {
      let sql = "INSERT INTO users(name,emailid,username,password) values(?,?,?,?)";
      this.db.executeSql(sql, [name, email,username,password]).then((data) => {
        console.log("user insert success");
        resolve(data);
      }, (error) => {
        reject(error);
      });
    });
  }


  getAllImage(){

    console.log("Inside getAllImages");
    return new Promise((resolve,reject)=>{
      let images=[];
      let sql="select * from imagestore";
      this.db.executeSql(sql,[]).then((data)=>{
        if(data.rows.length>0){
          for(var i=0;i<data.rows.length;i++){

            //console.log("Name direct from db:"+data.rows.item(i).name);
            images.push({
              name : data.rows.item(i).name,
              filelocation:data.rows.item(i).filelocation,
              description : data.rows.item(i).description,
              dateT:data.rows.item(i).dateT
            });
          }
        }
       // console.log("Just after pushing data in images:"+images);
        resolve(images);
      },(error)=>{
        reject(images);
      });
    });
  }


  
  DeleteAllImages(){
    return new Promise((resolve,reject)=>{
      let sql="delete from imagestore;"
      this.db.executeSql(sql,[]).then((data)=>{
        resolve(data);
        console.log("Data deleted");
      },(error)=>{
        reject(error);
      })
    })
  }

  DeleteAllUser(){
    return new Promise((resolve,reject)=>{

      let sql="delete from USERS";
      let sql1="UPDATE SQLITE_SEQUENCE SET SEQ=0 WHERE NAME='users';";
      this.db.executeSql(sql1,[]).then((data)=>{
        
        resolve(data);
      },(error)=>{
        reject(error);
      })
      this.db.executeSql(sql,[]).then((data)=>{
        console.log("Data deleted");
        resolve(data);
      },(error)=>{
        reject(error);
      })
    });
  }


  getUser(name,pass){
    console.log("Check name:"+name+pass);

    return new Promise((resolve,reject)=>{
      let sql="select * from users where name=? and password=?";
      this.db.executeSql(sql,[name,pass]).then((data)=>{
        console.log("Query executed");
        if(data.rows.length==1)
        resolve(data.rows.item(0).id);
        else resolve(-1);
      },(error)=>{
        reject(error);
      })
    });
  }

  getLastUser(){
    
    return new Promise((resolve,reject)=>{
      
       var arr:string[];
      let sql="select * from users order by id desc limit 1;";
      this.db.executeSql(sql,[]).then((data)=>{
        console.log("*** inside getLastUser of database.ts:Id :"+data.rows.item(0).id+"  Name:"+data.rows.item(0).username)
        arr=[data.rows.item(0).id,data.rows.item(0).username];
        //arr[0]=data.rows.item(0).username;

        console.log("*** inside getLastUser of database.ts:"+arr[0]+" "+arr[1]);
        resolve(arr);
      },(error)=>reject(error));
    })
  }

  GetAllUsers() {
    return new Promise((resolve, reject) => {
      let developers = [];
      let sql = "SELECT * FROM users";
      this.db.executeSql(sql, []).then((data) => {
        if (data.rows.length > 0) {
          for (var i = 0; i < data.rows.length; i++) {
            developers.push({
              id: data.rows.item(i).id,
              name: data.rows.item(i).name,
              email: data.rows.item(i).emailid
            });
          }
        }
        resolve(developers);
      }, (error) => {
        reject(developers);
      });
    });
  }

}