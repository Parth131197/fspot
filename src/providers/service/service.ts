import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';


@Injectable()
export class ServiceProvider {

  loginId :any;
  loginName:string;
  constructor(public http: HttpClient) {
    console.log('Hello ServiceProvider Provider');
    this.loginId=0;
    this.loginName="";
  }

  setService(id,name){
    console.log("***Inside serviceProvider : Id:"+id+"  Name:"+name);
    this.loginId=id;
    this.loginName=name;
  }

  getServiceName(){
    return this.loginName;
  }

  getServiceId(){
    return this.loginId;
  }
}
