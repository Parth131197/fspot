import { Component ,ViewChild} from '@angular/core';
import { Nav,Platform, MenuController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { HomePage } from '../pages/home/home';
import { FirstPage } from '../pages/first/first';
import {MySpotsPage} from '../pages/my-spots/my-spots'
@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;
  rootPage:any = HomePage;

  constructor(private platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen,
    private menuCtrl:MenuController) {

      



    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
    });
  }



  exitApplication(){
    this.platform.exitApp();
  }

  goToHome(){
    console.log("Inside goToHome");
    this.menuCtrl.close();
    this.nav.setRoot(FirstPage);
  }

  MyFavSpots(){
    this.menuCtrl.close();
    this.nav.setRoot(MySpotsPage);
  }
  goToLogin(){
    console.log("Inside goToLogin");
    this.menuCtrl.close();
    this.nav.setRoot(HomePage);
  }
}

